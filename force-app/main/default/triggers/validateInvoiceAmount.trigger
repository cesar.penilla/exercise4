trigger validateInvoiceAmount on Invoice__c (before insert, before update) {
	Decimal invoAmount = 0;
    Decimal expAmount = 0;
    List<AggregateResult> queryAmountInv = [SELECT SUM(Total_Amount__c) TotalAmount
                                     		FROM Invoice__c 
                                     		WHERE Invoice__c.Expense_Report__c =: Trigger.new[0].Expense_Report__c ];
    
    if(queryAmountInv[0].get('TotalAmount') != null){
        invoAmount = (Decimal) queryAmountInv[0].get('TotalAmount');
    }
    
    	
    List<AggregateResult> queryAmountExp = [SELECT SUM(Total_Amount__c) expAmount
                                     		FROM Expense_Report__c 
                                     		WHERE ID =: Trigger.new[0].Expense_Report__c ];
    
    expAmount = (Decimal) queryAmountExp[0].get('expAmount');
    
   /* for(Invoice__c invo: Trigger.new){
        invoAmount += invo.Total_Amount__c;
        if(invoAmount > expAmount){
        	invo.addError('the total amount of the invoices must not be greater than the amount of the expense report');
    	}
    }*/

}