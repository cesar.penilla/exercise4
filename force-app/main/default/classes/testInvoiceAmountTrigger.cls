@isTest
public with sharing class testInvoiceAmountTrigger {
    
    @isTest 
    static void testInvoiceAmount(){
        Expense_Report__c expReport = new Expense_Report__c();
        expReport.Name = 'Test';
        expReport.Date__c = Date.newInstance(2022, 02, 05);
        insert expReport;
        
        Invoice__c invoice = new Invoice__c();
        invoice.Total_Amount__c = 200;
        invoice.Name = 'Test Invo';
        invoice.Expense_Report__c = expReport.Id;
        
        Test.startTest();
        insert invoice;
        Test.stopTest();
        
        System.debug(invoice.Total_Amount__c);
        System.debug(expReport.Total_Amount__c);
        //System.assertEquals(invoice.Total_Amount__c, expReport.Total_Amount__c);
    }
    
    @isTest 
    static void testInvoiceAmountNegative(){
        Expense_Report__c expReport = new Expense_Report__c();
        expReport.Name = 'TestN';
        expReport.Date__c = Date.newInstance(2022, 02, 05);
        insert expReport;
        
        Invoice__c invoice1 = new Invoice__c();
        invoice1.Total_Amount__c = 50;
        invoice1.Name = 'Test Invo1 N';
        invoice1.Expense_Report__c = expReport.Id;
        insert invoice1;
        
        Invoice__c invoice = new Invoice__c();
        invoice.Total_Amount__c = 300;
        invoice.Name = 'Test Invo N';
        invoice.Expense_Report__c = expReport.Id;
        
        Test.startTest();
        //Database.insert(invoice, false);
        try{
            insert invoice;
        }catch(DmlException e){
             System.debug('waaaaasssseeee '+ e.getMessage());
        }
        
  
        Test.stopTest();
        /*System.debug(invoice.Total_Amount__c);
        //System.debug(expReport.Amount__c);
        List<Database.Error> errors = invoice.getErrors();
        System.debug('Error: '+ errors);
        System.assertEquals(1, errors.size()); // Assert Result
        Database.Error error = errors.get(0);
        System.assertEquals('the total amount of the invoices must not be greater than the amount of the expense report', error.getMessage()); // Validate message
        System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION, error.getStatusCode());*/
        
    }

}