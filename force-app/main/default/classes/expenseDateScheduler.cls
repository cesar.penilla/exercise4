global class expenseDateScheduler implements Schedulable {
    
    public Messaging.SingleEmailMessage email;
    public string [] toAddresEma = New string[]{'cesar.penilla@oktana.com'};
        
   global void execute(SchedulableContext SC) {
      setExpenseDateScheduler(); 
   }
    
    public void setExpenseDateScheduler(){
        List<Expense_Report__c> listExpenses = new List<Expense_Report__c>();
        listExpenses = [SELECT Date__c FROM Expense_Report__c];
        
        for(Expense_Report__c expReport : listExpenses){
            Date auxDate = expReport.Date__c;
            auxDate.addDays(15);
            if(expReport.Date__c > auxDate){
                email.setSubject('Approval process date expired');
                email.setPlainTextBody('approval process date expired');
                email.setToAddresses(toAddresEma);
                Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
            } else {
                email.setSubject('Please complete the approval process');
                email.setPlainTextBody('Please check the approval process pending');
                email.setToAddresses(toAddresEma);
                Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
            }
        }
    }
}