global class expenseDateScheduler2 implements Schedulable {
    
    public Messaging.SingleEmailMessage email;
    public string [] toAddresEma = New string[]{'cesar.penilla@oktana.com'};
        
   global void execute(SchedulableContext SC) {
      setExpenseDateScheduler(); 
   }
    
    public void setExpenseDateScheduler(){
        List<Expense_Report__c> listExpenses = new List<Expense_Report__c>();
        listExpenses = [SELECT Date__c FROM Expense_Report__c];
        
        for(Expense_Report__c expReport : listExpenses){
            Date auxDate = expReport.Date__c.addDays(15);
            //auxDate.addDays(15);
            if(System.today() > auxDate){
                System.debug('Approval process date expired '+ auxDate + ' - ' + expReport.Date__c);
                /*email.setSubject('Approval process date expired');
                email.setPlainTextBody('approval process date expired');
                email.setToAddresses(toAddresEma);
                Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});*/
            } else {
                System.debug('Please complete the approval process '+ auxDate + ' - ' + expReport.Date__c);
                /*email.setSubject('Please complete the approval process');
                email.setPlainTextBody('Please check the approval process pending');
                email.setToAddresses(toAddresEma);
                Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});*/
            }
        }
    }
}